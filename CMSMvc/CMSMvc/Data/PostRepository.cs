﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMSMvc.Models;

namespace CMSMvc.Data
{
    public class PostRepository:IPostRepository
    {
        public Post Get(string id)
        {
            using (var db = new CmsContext())
            {
                return db.Posts.Include("Author").SingleOrDefault(post => post.Id == id);
            }
        }

        public void Edit(string id, Models.Post updatedItem)
        {
            using (var db = new CmsContext())
            {
                var post = db.Posts.SingleOrDefault(p => p.Id == id);

                if (post == null)
                {
                    throw new KeyNotFoundException("A Post with the id of " + id + "does not exist in the data store.");
                }              

                post.Id = updatedItem.Id.MakeURLFriendly();
                post.Title = updatedItem.Title;
                post.Published = updatedItem.Published;
                post.Conten = updatedItem.Conten;
                post.Tags = updatedItem.Tags
                    .Select(tag => tag.MakeURLFriendly()).ToList();

                db.SaveChanges();

            }
        }

        public void Delete(String id)
        {
            using (var db = new CmsContext())
            {
                var post = db.Posts.SingleOrDefault(p => p.Id == id);

                if (post == null)
                {
                    throw new KeyNotFoundException("The post with the id of" + id + "");
                }

                db.Posts.Remove(post);
                db.SaveChanges();
            }
        }

        public void Create(Models.Post model)
        {
              using (var db = new CmsContext())
              {
                  var post = db.Posts.SingleOrDefault(p => p.Id == model.Id);

                  if (post != null)
                  {
                      throw new ArgumentException("A post with the id of" + model.Id + " already exist in database.");
                  }

                 

                  db.Posts.Add(model);
                  db.SaveChanges();
              }
        }

        public IEnumerable<Models.Post> GetAll()
        {
            using (var db = new CmsContext())
            {
                return db.Posts.Include("Author")
                    .OrderByDescending(post => post.Created).ToArray();
            }
        }
    }
}