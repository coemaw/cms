﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMSMvc.Data
{
    public class TagRepository : ITagsRepository
    {
        public IEnumerable<string> GetAll()
        {
            using(var db = new CmsContext()){

                var tagCollection = db.Posts.Select(p => p.combineTags).ToList();

                return string.Join(",", tagCollection).Split(',').Distinct();

                //return db.Posts.ToList().SelectMany(post => post.Tags).Distinct();
            }
        }

        public string Get(string tag)
        {
            using(var db = new CmsContext()){

                var posts = db.Posts.Where(post => post.combineTags.Contains(tag)).ToList();

                  posts = posts.Where(p=>
                     p.Tags.Contains(tag, StringComparer.CurrentCultureIgnoreCase)).ToList();

                 if(!posts.Any()){
                     throw new KeyNotFoundException("The tag " + tag + " does not exist.");
                 }

                return tag.ToLower();
             }
        }

        public void Edit(string existingTag, string newTag)
        {
             using(var db = new CmsContext()){

                 var posts = db.Posts.Where(post => post.combineTags.Contains(existingTag)).ToList();

                  posts = posts.Where(p=>
                     p.Tags.Contains(existingTag,StringComparer.CurrentCultureIgnoreCase)).ToList();

                 if(!posts.Any()){
                     throw new KeyNotFoundException("The tag " + existingTag + " does not exist.");
                 }

                 foreach(var post in posts){
                     post.Tags.Remove(existingTag);
                     post.Tags.Add(newTag);
                 }

                 db.SaveChanges();
             }
        }

        public void Delete(string tag)
        {
             using(var db = new CmsContext()){

                 var posts = db.Posts.Where(post => post.combineTags.Contains(tag)).ToList();

                  posts = posts.Where(p=>
                     p.Tags.Contains(tag,StringComparer.CurrentCultureIgnoreCase)).ToList();

                 if(!posts.Any()){
                     throw new KeyNotFoundException("The tag " + tag + " does not exist.");
                 }

                 foreach(var post in posts){
                     post.Tags.Remove(tag);                    
                 }

                 db.SaveChanges();
             }
        }
    }
}