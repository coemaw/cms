﻿using CMSMvc.Data;
using CMSMvc.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMSMvc.App_Start
{
    public class AuthDbConfig
    {
        public static void RegisterAdmin()
        {
          
            using (var users = new UserRepository())
            {
                var user = users.GetUserByNameAsync("admin");

                if (user == null)
                {
                    var adminUser = new CmsUser
                    {
                        UserName = "admin",
                        Email = "admin@cms.com",
                        DisplayName = "Administrator"
                    };

                    users.CreateAsync(adminUser, "password");
                }
            }

            using (var role = new RoleRepository())
            {
                if (role.GetRoleByNameAsync("admin") == null)
                {
                    role.CreateAsync(new IdentityRole("admin"));
                }

                if (role.GetRoleByNameAsync("author") == null)
                {
                    role.CreateAsync(new IdentityRole("author"));
                }

                if (role.GetRoleByNameAsync("editor") == null)
                {
                    role.CreateAsync(new IdentityRole("editor"));
                }

            }
        }

    }
}