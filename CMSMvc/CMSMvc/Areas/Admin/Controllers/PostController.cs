﻿using CMSMvc.Data;
using CMSMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMSMvc.Areas.Admin.Controllers
{
    // /admin /post
    [RouteArea("Admin")]
    [RoutePrefix("post")]
    public class PostController : Controller
    {

        private IPostRepository repo;

        public PostController()
            : this(new PostRepository()) { }

        public PostController(IPostRepository _repo)
        {
            repo = _repo;
        }

        // GET: Admin/Post
        [Route("")]
        public ActionResult Index()
        {
            var posts = repo.GetAll();

            return View(posts);
        }

        [HttpGet]
        [Route("create")]
        public ActionResult Create()
        {            

            return View(new Post());
        }

        [HttpPost]
        [Route("create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (string.IsNullOrWhiteSpace(model.Id))
            {
                model.Id = model.Title;
            }

            model.Id = model.Id.MakeURLFriendly();
            model.Tags = model.Tags.Select(tag => tag.MakeURLFriendly()).ToList();
            model.Created = DateTime.Now;
            model.AuthorId = "7a0607ce-5f06-4d9f-a99b-caee40e1fc8d";
            try
            {

                repo.Create(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);

                return View(model);
            }

            //TO DO Update Model
        }

        [HttpGet]
        [Route("edit/{postId}")]
        public ActionResult Edit(string postId)
        {
            var post = repo.Get(postId);

            if (post == null)
            {
                return HttpNotFound();
            }
          
            return View(post);
        }

        [HttpPost]
        [Route("edit/{postId}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string postId,Post model)
        {
            //var post = repo.Get(postId);

            //if (post == null)
            //    return HttpNotFound();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (string.IsNullOrWhiteSpace(model.Id))
            {
                model.Id = model.Title;
            }

            model.Id = model.Id.MakeURLFriendly();
            model.Tags = model.Tags.Select(tag => tag.MakeURLFriendly()).ToList();

            try
            {
                repo.Edit(postId, model);

                return RedirectToAction("Index");
            }
            catch (KeyNotFoundException ex)
            {
                return HttpNotFound();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
           
            //TO DO Update Model
        }


        [HttpGet]
        [Route("delete/{postId}")]
        public ActionResult Delete(string postId)
        {
            var post = repo.Get(postId);

            if (post == null)
            {
                return HttpNotFound();
            }

            return View(post);
        }

        [HttpPost]
        [Route("delete/{postId}")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string postId,string foo)
        {                    
            try
            {
                repo.Delete(postId);

                return RedirectToAction("Index");
            }
            catch (KeyNotFoundException ex)
            {
                return HttpNotFound();
            }
           

            //TO DO Update Model
        }


    }
}