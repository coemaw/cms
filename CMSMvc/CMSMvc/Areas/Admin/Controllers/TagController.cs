﻿using CMSMvc.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMSMvc.Areas.Admin.Controllers
{
    [RouteArea("admin")]
    [RoutePrefix("tag")]
    public class TagController : Controller
    {
        
        private ITagsRepository TagRepo;

        public TagController() : this(new TagRepository()) { }

        public TagController(ITagsRepository _TagRepo)
        {
            TagRepo = _TagRepo;
        }

        [Route("")]
        public ActionResult Index()
        {
            var tags = TagRepo.GetAll();

            return View(tags);
        }

        [HttpGet]
        [Route("edit/{tag}")]
        public ActionResult Edit(string tag)
        {
            try
            {
                var model = TagRepo.Get(tag);
                return View(model: model);
            }
            catch (KeyNotFoundException ex)
            {
                return HttpNotFound();
            }

            if (TagRepo.Get(tag) == tag)
            {
                return HttpNotFound();
            }

            return View(tag);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("edit/{tag}")]
        public ActionResult Edit(string tag,string newTag)
        {
            var tags = TagRepo.GetAll();

            if (!tags.Contains(tag))
            {
                return HttpNotFound();
            }

            if (tags.Contains(newTag))
            {
                return RedirectToAction("Index");
            }

            if (string.IsNullOrWhiteSpace(newTag))
            {
                ModelState.AddModelError("Key","New tag value cannot be empty.");

                return View(model: tag);
            }

            TagRepo.Edit(tag, newTag);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("delete/{tag}")]
        public ActionResult Delete(string tag)
        {
           try
            {
                var model = TagRepo.Get(tag);
                return View(model: model);
            }
            catch (KeyNotFoundException ex)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("delete/{tag}")]
        public ActionResult Delete(string tag,string foo)
        {
            try
            {
                TagRepo.Delete(tag);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return HttpNotFound();
            }          

            
        }
        
    }
}